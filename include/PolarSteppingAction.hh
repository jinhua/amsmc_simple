
#ifndef PolarSteppingAction_h
#define PolarSteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"
#include "G4UserEventAction.hh"

class PolarEventAction;

class G4LogicalVolume;

/// Stepping action class
/// 

class PolarSteppingAction : public G4UserSteppingAction
{
  public:
    PolarSteppingAction();
    virtual ~PolarSteppingAction();

    // method from the base class
    virtual void UserSteppingAction(const G4Step*);

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
