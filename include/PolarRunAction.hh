#ifndef PolarRunAction_h
#define PolarRunAction_h 1
#include "PolarEventAction.hh"
#include "G4UserRunAction.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
class G4Run;

/// Run action class

class PolarRunAction : public G4UserRunAction
{
  public:
    PolarRunAction(G4String name);
    virtual ~PolarRunAction();

    virtual void BeginOfRunAction(const G4Run* run);
    virtual void   EndOfRunAction(const G4Run* run);

     private:
    G4String fname;
  
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
