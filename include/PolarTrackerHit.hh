#ifndef PolarTrackerHit_h
#define PolarTrackerHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "tls.hh"


class PolarTrackerHit : public G4VHit
{
  public:
    PolarTrackerHit();
    PolarTrackerHit(const PolarTrackerHit&);
    virtual ~PolarTrackerHit();

    // operators
    const PolarTrackerHit& operator=(const PolarTrackerHit&);
    G4bool operator==(const PolarTrackerHit&) const;

    inline void* operator new(size_t);
    inline void  operator delete(void*);

    // methods from base class
    virtual void Draw();
    virtual void Print();

    // Set methods
    void SetParentID  (G4int parent)      { fParentID = parent; };
    void SetTrackID  (G4int track)      { fTrackID = track; };
    void SetLayerNb(G4int layer)      { fLayerNb = layer; };
    void SetEdep     (G4double de)      { fEdep = de; };
    void SetPos      (G4ThreeVector xyz){ fPos = xyz; };
    void SetEnergy     (G4double ener)      { fEnergy = ener; };
    void SetDir      (G4ThreeVector xyz){ fDir = xyz; };
    void SetParticleName    (G4String pari){ fparticlename = pari; };

    // Get methods
    G4int GetParentID() const     { return fParentID; };
    G4int GetTrackID() const     { return fTrackID; };
    G4int GetLayerNb() const   { return fLayerNb; };
    G4double GetEdep() const     { return fEdep; };
    G4ThreeVector GetPos() const { return fPos; };
    G4ThreeVector GetDir() const { return fDir; };
    G4double GetEnergy() const { return fEnergy; };
    G4String  GetPaticleName() const { return fparticlename; };

  private:
    G4int         fParentID;
      G4int         fTrackID;
      G4int         fLayerNb;
      G4double      fEdep;
      G4ThreeVector fPos;
      G4ThreeVector fDir;
     G4double      fEnergy;
    G4String fparticlename;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

typedef G4THitsCollection<PolarTrackerHit> PolarTrackerHitsCollection;

extern G4ThreadLocal G4Allocator<PolarTrackerHit>* PolarTrackerHitAllocator;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void* PolarTrackerHit::operator new(size_t)
{
  if(!PolarTrackerHitAllocator)
      PolarTrackerHitAllocator = new G4Allocator<PolarTrackerHit>;
  return (void *) PolarTrackerHitAllocator->MallocSingle();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

inline void PolarTrackerHit::operator delete(void *hit)
{
  PolarTrackerHitAllocator->FreeSingle((PolarTrackerHit*) hit);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
