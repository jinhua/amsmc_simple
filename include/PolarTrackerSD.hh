#ifndef PolarTrackerSD_h
#define PolarTrackerSD_h 1

#include "G4VSensitiveDetector.hh"

#include "PolarTrackerHit.hh"

#include <vector>

class G4Step;
class G4HCofThisEvent;


class PolarTrackerSD : public G4VSensitiveDetector
{
  public:
    PolarTrackerSD(const G4String& name, 
                const G4String& hitsCollectionName);
    virtual ~PolarTrackerSD();
  
    // methods from base class
    virtual void   Initialize(G4HCofThisEvent* hitCollection);
    virtual G4bool ProcessHits(G4Step* step, G4TouchableHistory* history);
    virtual void   EndOfEvent(G4HCofThisEvent* hitCollection);

  private:
    PolarTrackerHitsCollection* fHitsCollection;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
