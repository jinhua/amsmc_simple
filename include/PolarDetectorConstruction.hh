#ifndef PolarDetectorConstruction_h
#define PolarDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
//#include "tls.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;

class G4Material;
class G4UserLimits;
class G4FieldManager;
class G4MagneticField;
class G4UniformMagField;
class G4TransportationManager;
/// Detector construction class to define materials and geometry.

class PolarDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    PolarDetectorConstruction();
    virtual ~PolarDetectorConstruction();

   virtual  G4VPhysicalVolume* Construct();
 //    virtual void ConstructSDandField();
 //   void SetMaxStep (G4double );
//    G4LogicalVolume* GetScoringVolume() const { return fScoringVolume; }

     virtual void ConstructSDandField();

  protected:
  //  G4LogicalVolume*  fScoringVolume;
    G4int fNbOfLayers;

    G4LogicalVolume*   fLogicTarget;     // pointer to the logical Target
    G4LogicalVolume**  fLogicLayer;    // pointer to the logical Chamber
    G4LogicalVolume*  flogicInner;
    G4LogicalVolume*  flogicMagnetic; //  G4UserLimits* fStepLimit;
//   static G4ThreadLocal G4GlobalMagFieldMessenger*  fMagFieldMessenger; 
  G4UserLimits* fStepLimit;            // pointer to user step limits

     void BuildTrLayer(G4int ilayer,G4double LayerR,G4double LayerZ,G4double Zposi,G4LogicalVolume* mothervolume);



};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
    
