#ifndef PolarActionInitialization_h
#define PolarActionInitialization_h 1

#include "G4VUserActionInitialization.hh"
#include "globals.hh"
class B4DetectorConstruction;


class PolarActionInitialization : public G4VUserActionInitialization
{
  public:
    PolarActionInitialization(G4String name);
    virtual ~PolarActionInitialization();

    virtual void BuildForMaster() const;
    virtual void Build() const;

     G4String fname;
};

#endif

    
