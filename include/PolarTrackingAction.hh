
#ifndef PolarTrackingAction_h
#define PolarTrackingAction_h 1

#include "G4UserTrackingAction.hh"

class PolarTrackingAction : public G4UserTrackingAction 
{
public:
  PolarTrackingAction();
  virtual ~PolarTrackingAction(){};
   
  virtual void PreUserTrackingAction(const G4Track*);
  virtual void PostUserTrackingAction(const G4Track*);
  
};

#endif
