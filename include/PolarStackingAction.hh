#ifndef PolarStackingAction_H
#define PolarStackingAction_H 1

#include "globals.hh"
#include "G4UserStackingAction.hh"

class G4Track;
class G4VHitsCollection;

class PolarStackingAction : public G4UserStackingAction
{
public:
  PolarStackingAction();
  virtual ~PolarStackingAction();

public:
  virtual G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track* aTrack);
  virtual void NewStage();
  virtual void PrepareNewEvent();

private:
  G4int fStage;
};

#endif

