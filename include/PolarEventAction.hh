#ifndef PolarEventAction_h
#define PolarEventAction_h 1

#include "G4UserEventAction.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

/// Event action class

class PolarEventAction : public G4UserEventAction
{
  public:
    PolarEventAction();
    virtual ~PolarEventAction();

    virtual void  BeginOfEventAction(const G4Event* );
    virtual void    EndOfEventAction(const G4Event* );

 
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
