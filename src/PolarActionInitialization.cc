#include "PolarActionInitialization.hh"
#include "PolarPrimaryGeneratorAction.hh"
#include "PolarRunAction.hh"
#include "PolarTrackingAction.hh"
#include "PolarStackingAction.hh"
#include "PolarEventAction.hh"
#include "PolarSteppingAction.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarActionInitialization::PolarActionInitialization(G4String name)
 : G4VUserActionInitialization(),
  fname(name)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarActionInitialization::~PolarActionInitialization()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PolarActionInitialization::BuildForMaster() const
{
  SetUserAction(new PolarRunAction(fname));
}

//....oooOO0OOooo........oooOO0OOyooo........oooOO0OOooo........oooOO0OOooo......

void PolarActionInitialization::Build() const
{
  SetUserAction(new PolarPrimaryGeneratorAction);
  SetUserAction(new PolarRunAction(fname));
  SetUserAction(new PolarEventAction);
  SetUserAction(new PolarStackingAction);
  SetUserAction(new PolarTrackingAction);
  SetUserAction(new PolarSteppingAction);
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
