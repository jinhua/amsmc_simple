#include "PolarPrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"

#include "Randomize.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarPrimaryGeneratorAction::PolarPrimaryGeneratorAction()
 : G4VUserPrimaryGeneratorAction()
{
//    G4cout<<"PolarPrimaryGeneratorAction"<<G4endl;
  G4int nofParticles = 1;
  fParticleGun = new G4ParticleGun(nofParticles);

  // default particle kinematic

  G4ParticleDefinition* particleDefinition 
    = G4ParticleTable::GetParticleTable()->FindParticle("e-");

  fParticleGun->SetParticleDefinition(particleDefinition);
  
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,-1));
  fParticleGun->SetParticleEnergy(10*GeV);
  fParticleGun->SetParticlePosition(G4ThreeVector(0,0,195*cm));

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarPrimaryGeneratorAction::~PolarPrimaryGeneratorAction()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PolarPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
//    G4cout<<"GeneratePrimaries"<<G4endl;

G4double size = 390*cm;
  G4double x0 = size * (G4UniformRand()-0.5);
  G4double y0 = size * (G4UniformRand()-0.5);
 
  fParticleGun->SetParticlePosition(G4ThreeVector(x0,y0,195*cm));

G4ThreeVector pDir(0, 0, 1);
G4double Cos=G4UniformRand();
pDir.setTheta(acos(-1*pow(Cos,0.5)));
pDir.setPhi(360*degree*G4UniformRand());

fParticleGun->SetParticleMomentumDirection(pDir);
/*
G4double energy=1.0/G4RandExponential::shoot(1)*100;
G4cout<<"energy="<<energy<<" MeV"<<G4endl;
 fParticleGun->SetParticleEnergy(energy*MeV);
*/
 fParticleGun->GeneratePrimaryVertex(anEvent);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
