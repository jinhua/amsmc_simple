#include "PolarSteppingAction.hh"
#include "PolarEventAction.hh"
#include "PolarDetectorConstruction.hh"

#include "G4Step.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"
#include "g4analysis.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
PolarSteppingAction::PolarSteppingAction()
: G4UserSteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarSteppingAction::~PolarSteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PolarSteppingAction::UserSteppingAction(const G4Step* step)
{
//G4cout<<"UserSteppingAction"<<G4endl;

 // G4Track * theTrack = step->GetTrack();
//  if(theTrack->GetTrackStatus()!=fAlive) { return; }

/*G4LogicalVolume* volume 
    = step->GetPreStepPoint()->GetTouchableHandle()
      ->GetVolume()->GetLogicalVolume();
      
   if(   )
  {
    RE01TrackInformation* trackInfo
     = static_cast<RE01TrackInformation*>(theTrack->GetUserInformation());
    if(trackInfo->GetSuspendedStepID()>-1);
    else
    {
      trackInfo->SetSuspendedStepID(theTrack->GetCurrentStepNumber());
      theTrack->SetTrackStatus(fSuspend);
    }
  }*/


G4double energy;
G4ThreeVector Dir;
G4ThreeVector Pos;

if(step->GetPreStepPoint()->GetPosition().z() == 195 *CLHEP::cm){
energy=step->GetPreStepPoint()->GetTotalEnergy ();
Dir=step->GetPreStepPoint()->GetMomentumDirection ();
Pos=step->GetPreStepPoint()->GetPosition();

auto analysisManager = G4AnalysisManager::Instance();
analysisManager->FillNtupleDColumn(1,0,energy);
analysisManager->FillNtupleDColumn(1,1,Dir.x());
analysisManager->FillNtupleDColumn(1,2,Dir.y());
analysisManager->FillNtupleDColumn(1,3,Dir.z());
analysisManager->FillNtupleDColumn(1,4,Pos.x()/10);
analysisManager->FillNtupleDColumn(1,5,Pos.y()/10);
analysisManager->FillNtupleDColumn(1,6,Pos.z()/10);
analysisManager->AddNtupleRow(1);
}

 

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

