#include "PolarTrackerHit.hh"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

#include <iomanip>

G4ThreadLocal G4Allocator<PolarTrackerHit>* PolarTrackerHitAllocator=0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarTrackerHit::PolarTrackerHit()
 : G4VHit(),
   fParentID(-1),
   fTrackID(-1),
   fLayerNb(-1),
   fEdep(0.),
   fPos(G4ThreeVector()),
   fDir(G4ThreeVector()), 
   fEnergy(0.),
   fparticlename("")
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarTrackerHit::~PolarTrackerHit() {}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarTrackerHit::PolarTrackerHit(const PolarTrackerHit& right)
  : G4VHit()
{
    fParentID   = right.fParentID;
  fTrackID   = right.fTrackID;
  fLayerNb = right.fLayerNb;
  fEdep      = right.fEdep;
  fPos       = right.fPos;
  fDir       = right.fDir;
  fEnergy     = right.fEnergy;
  fparticlename=right.fparticlename;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

const PolarTrackerHit& PolarTrackerHit::operator=(const PolarTrackerHit& right)
{
    fParentID   = right.fParentID;
  fTrackID   = right.fTrackID;
  fLayerNb = right.fLayerNb;
  fEdep      = right.fEdep;
  fPos       = right.fPos;
  fDir       = right.fDir;
  fEnergy     = right.fEnergy;
  fparticlename=right.fparticlename;
  return *this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool PolarTrackerHit::operator==(const PolarTrackerHit& right) const
{
  return ( this == &right ) ? true : false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PolarTrackerHit::Draw()
{
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
  {
    G4Circle circle(fPos);
    circle.SetScreenSize(4.);
    circle.SetFillStyle(G4Circle::filled);
    G4Colour colour(1.,0.,0.);
    G4VisAttributes attribs(colour);
    circle.SetVisAttributes(attribs);
    pVVisManager->Draw(circle);
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PolarTrackerHit::Print()
{
  G4cout
     << "  trackID: " << fTrackID << " layerNb: " << fLayerNb
     << "Edep: "
     << std::setw(7) << G4BestUnit(fEdep,"Energy")
     << " Position: "
     << std::setw(7) << G4BestUnit( fPos,"Length")
     << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
