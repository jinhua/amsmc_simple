#include "PolarRunAction.hh"
#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4GenericAnalysisManager.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

using G4AnalysisManager = G4GenericAnalysisManager;

PolarRunAction::PolarRunAction(G4String name)
 : G4UserRunAction(),
  fname(name)
 { 
  // set printing event number per each 100 events
  G4RunManager::GetRunManager()->SetPrintProgress(1000);     

 auto analysisManager = G4AnalysisManager::Instance();

  analysisManager->SetVerboseLevel(1);
  analysisManager->SetNtupleMerging(true);

analysisManager->SetFileName("Ntuple_electron_Galactic_NoMagn_10GeV_"+fname);

    analysisManager->CreateNtuple("Hits", "Hits");
    analysisManager->CreateNtupleIColumn(0,"EventID");  
       analysisManager->CreateNtupleIColumn(0,"NTrack");
    analysisManager->CreateNtupleIColumn(0,"NHit");  // column Id = 1
    analysisManager->CreateNtupleDColumn(0,"Edep"); // column Id = 2
     analysisManager->CreateNtupleDColumn(0,"vertexX");
     analysisManager->CreateNtupleDColumn(0,"vertexY");
     analysisManager->CreateNtupleDColumn(0,"vertexZ");
     analysisManager->CreateNtupleDColumn(0,"Layer1X");
     analysisManager->CreateNtupleDColumn(0,"Layer1Y");
     analysisManager->CreateNtupleDColumn(0,"Layer2X");
     analysisManager->CreateNtupleDColumn(0,"Layer2Y");
     analysisManager->CreateNtupleDColumn(0, "Layer3X");
     analysisManager->CreateNtupleDColumn(0, "Layer3Y");
     analysisManager->CreateNtupleDColumn(0, "Layer4X");
     analysisManager->CreateNtupleDColumn(0, "Layer4Y");
     analysisManager->CreateNtupleDColumn(0, "Layer5X");
     analysisManager->CreateNtupleDColumn(0, "Layer5Y");
     analysisManager->CreateNtupleDColumn(0, "Layer6X");
     analysisManager->CreateNtupleDColumn(0, "Layer6Y");
     analysisManager->CreateNtupleDColumn(0, "Layer7X");
     analysisManager->CreateNtupleDColumn(0, "Layer7Y");
     analysisManager->CreateNtupleDColumn(0, "Layer8X");
     analysisManager->CreateNtupleDColumn(0, "Layer8Y");
     analysisManager->CreateNtupleDColumn(0, "Layer9X");
     analysisManager->CreateNtupleDColumn(0, "Layer9Y");
 analysisManager->FinishNtuple();
    analysisManager->CreateNtuple("Pri", "Pri");
    analysisManager->CreateNtupleDColumn(1,"PriEnergy"); 
    analysisManager->CreateNtupleDColumn(1,"PriDirX");
     analysisManager->CreateNtupleDColumn(1,"PriDirY");
    analysisManager->CreateNtupleDColumn(1,"PriDirZ");
    analysisManager->CreateNtupleDColumn(1,"PriPosiX");
    analysisManager->CreateNtupleDColumn(1,"PriPosiY");
    analysisManager->CreateNtupleDColumn(1,"PriPosiZ");
    analysisManager->FinishNtuple();

//analysisManager->SetNtupleFileName(0,"Hits");
//analysisManager->SetNtupleFileName(1,"Pri");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
PolarRunAction::~PolarRunAction()
{
delete  G4AnalysisManager::Instance();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PolarRunAction::BeginOfRunAction(const G4Run*)
{ 
  //inform the runManager to save random number seed
  G4RunManager::GetRunManager()->SetRandomNumberStore(false);
 auto analysisManager = G4AnalysisManager::Instance();
analysisManager->OpenFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PolarRunAction::EndOfRunAction(const G4Run* )
{
 auto analysisManager = G4AnalysisManager::Instance();
analysisManager->Write();
analysisManager->CloseFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
