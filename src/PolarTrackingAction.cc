#include "PolarTrackingAction.hh"
#include "PolarTrajectory.hh"
#include "PolarTrackInformation.hh"

#include "G4TrackingManager.hh"
#include "G4Track.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 
PolarTrackingAction::PolarTrackingAction()
:G4UserTrackingAction()
{

}

//PolarTrackingAction::~PolarTrackingAction() {}

void PolarTrackingAction::PreUserTrackingAction(const G4Track* aTrack)
{
//G4cout<<"PreUserTrackingAction"<<G4endl;

  PolarTrackInformation* trackInfo = 
    static_cast<PolarTrackInformation*>(aTrack->GetUserInformation());

  if(trackInfo->GetTrackingStatus()>0)
  {
    fpTrackingManager->SetStoreTrajectory(true);
    fpTrackingManager->SetTrajectory(new PolarTrajectory(aTrack));
  }
  else
  { fpTrackingManager->SetStoreTrajectory(false); }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 
void PolarTrackingAction::PostUserTrackingAction(const G4Track* aTrack)
{
G4cout<<"PostUserTrackingAction"<<G4endl;
  G4TrackVector* secondaries = fpTrackingManager->GimmeSecondaries();
  if(secondaries)
  {
    PolarTrackInformation* info = 
      (PolarTrackInformation*)(aTrack->GetUserInformation());
    size_t nSeco = secondaries->size();
    if(nSeco>0)
    {
      for(size_t i=0;i<nSeco;i++)
      {
if(info)  { 
 PolarTrackInformation* infoNew = new PolarTrackInformation(info);
        (*secondaries)[i]->SetUserInformation(infoNew);
}
      }
    }
  }
}


