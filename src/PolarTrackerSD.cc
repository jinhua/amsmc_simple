#include "PolarTrackerSD.hh"
#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarTrackerSD::PolarTrackerSD(const G4String& name,
                         const G4String& hitsCollectionName) 
 : G4VSensitiveDetector(name),
   fHitsCollection(NULL)
{
  collectionName.insert(hitsCollectionName);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarTrackerSD::~PolarTrackerSD() 
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PolarTrackerSD::Initialize(G4HCofThisEvent* hce)
{
  // Create hits collection
//G4cout<<"PolarTrackerSD::Initialize"<<G4endl;
  fHitsCollection 
    = new PolarTrackerHitsCollection(SensitiveDetectorName, collectionName[0]); 

  // Add this collection in hce

  G4int hcID 
    = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
  hce->AddHitsCollection( hcID, fHitsCollection );

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool PolarTrackerSD::ProcessHits(G4Step* aStep, 
                                     G4TouchableHistory*)
{  
//G4cout<<"ProcessHits"<<G4endl;
// energy deposit
  G4double edep = aStep->GetTotalEnergyDeposit();

  if (edep==0.) return false;

  PolarTrackerHit* newHit = new PolarTrackerHit();

  newHit->SetTrackID (aStep->GetTrack()->GetTrackID());
  newHit->SetLayerNb(aStep->GetPreStepPoint()->GetTouchableHandle()
                                               ->GetCopyNumber());
  newHit->SetEdep(edep);
  newHit->SetPos (aStep->GetPostStepPoint()->GetPosition());
  newHit->SetDir(aStep->GetPostStepPoint()->GetMomentumDirection());
  newHit->SetEnergy(aStep->GetPostStepPoint()->GetTotalEnergy());
  newHit->SetParticleName (aStep->GetTrack()->GetDefinition()->GetParticleName());
    newHit->SetParentID (aStep->GetTrack()->GetParentID());
  fHitsCollection->insert( newHit );


  return true;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PolarTrackerSD::EndOfEvent(G4HCofThisEvent*)
{
//G4cout<<"PolarTrackerSD::EndOfEvent"<<G4endl;

  if ( verboseLevel>1 ) { 
     G4int nofHits = fHitsCollection->entries();
     G4cout << G4endl
            << "-------->Hits Collection: in this event they are " << nofHits 
            << " hits in the tracker layers: " << G4endl;
     for ( G4int i=0; i<nofHits; i++ ) (*fHitsCollection)[i]->Print();
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
