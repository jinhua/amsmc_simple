#include "PolarStackingAction.hh"
#include "PolarTrackInformation.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4HCofThisEvent.hh"
#include "G4Track.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4SystemOfUnits.hh"    
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 
PolarStackingAction::PolarStackingAction()
  :G4UserStackingAction(),
   fStage(0)
{;}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 
PolarStackingAction::~PolarStackingAction()
{;}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 
G4ClassificationOfNewTrack PolarStackingAction
::ClassifyNewTrack(const G4Track * aTrack)
{
//    G4cout<<"ClassifyNewTrack"<<G4endl;
  G4ClassificationOfNewTrack classification = fUrgent;

  if(fStage==0)
  {
    PolarTrackInformation* trackInfo;
 /*   if(aTrack->GetTrackStatus()==fSuspend) // Track reached to calorimeter
    {
      trackInfo = (PolarTrackInformation*)(aTrack->GetUserInformation());
      trackInfo->SetTrackingStatus(0);
      trackInfo->SetSourceTrackInformation(aTrack);
      classification = fWaiting;
    }
    else*/

 if(aTrack->GetParentID()==0) // Primary particle
    {
      trackInfo = new PolarTrackInformation(aTrack);
      trackInfo->SetTrackingStatus(1);
      G4Track* theTrack = (G4Track*)aTrack;
      theTrack->SetUserInformation(trackInfo);
    }
 else  if(aTrack->GetParentID()==1) {
trackInfo = (PolarTrackInformation*)(aTrack->GetUserInformation());
      trackInfo->SetTrackingStatus(2);
      trackInfo->SetSourceTrackInformation(aTrack);
}
else if(aTrack->GetParentID()>1){//don't store thired track

 //   G4cout<<"Waiting event"<<G4endl;
trackInfo = (PolarTrackInformation*)(aTrack->GetUserInformation());
      trackInfo->SetTrackingStatus(-1);
      trackInfo->SetSourceTrackInformation(aTrack);
      classification = fWaiting;
}

  }
  return classification;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 
void PolarStackingAction::NewStage()
{
//    G4cout<<"NewStage"<<G4endl;
  if(stackManager->GetNUrgentTrack())
  {
    stackManager->TransferStackedTracks(fUrgent,fWaiting);
    stackManager->TransferOneStackedTrack(fWaiting,fUrgent);
    fStage++;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......     
void PolarStackingAction::PrepareNewEvent()
{//  G4cout<<"PrepareNewEvent"<<G4endl;
  fStage = 0; 
}
