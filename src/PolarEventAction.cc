#include "PolarEventAction.hh"
#include "PolarRunAction.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4ios.hh"
#include "g4analysis.hh"
#include "PolarTrackerHit.hh"
#include "PolarTrajectory.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarEventAction::PolarEventAction()
: G4UserEventAction()
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PolarEventAction::~PolarEventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PolarEventAction::BeginOfEventAction(const G4Event*)
{
//    G4cout<<"begin_event"<<G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PolarEventAction::EndOfEventAction(const G4Event* event)
{
  // get number of stored trajectories

  G4TrajectoryContainer* trajectoryContainer = event->GetTrajectoryContainer();
  G4int n_trajectories = 0;
  if (trajectoryContainer) n_trajectories = trajectoryContainer->entries();

  G4double vertexX=0,vertexY=0,vertexZ=0;
  G4int nvertex=0;
  for(G4int ntra=0;ntra<n_trajectories;ntra++) {
      PolarTrajectory* tra=static_cast<PolarTrajectory*>((*(trajectoryContainer))[ntra]);
      if(tra->GetParentID()==1) {
          vertexX+=tra->GetVertexPos().x();
          vertexY+=tra->GetVertexPos().y();
          vertexZ+=tra->GetVertexPos().z();
          nvertex++;
      }

   /*   if(tra->GetParentID()==0){
          for(G4int ipos=0;ipos<static_cast<PolarTrajectory*>((*(trajectoryContainer))[ntra])->GetPointEntries();ipos++) {
              G4VTrajectoryPoint *Trapoint = static_cast<PolarTrajectory *>((*(trajectoryContainer))[ntra])->GetPoint(
                      ipos);
              //the step infomation of a trajectory

          }
      }
*/

  }
  if(nvertex!=0) {
      vertexX = vertexX / nvertex / 10;
      vertexY = vertexY / nvertex / 10;
      vertexZ = vertexZ / nvertex / 10;
  }
  else{
      vertexX = -500;
      vertexY = -500;
      vertexZ = -500;
  }
//G4cout<<"vertex="<<vertexX<<"  "<<vertexY<<" "<<vertexZ<<G4endl;


  // periodic printing

  G4int eventID = event->GetEventID();
  if ( eventID < 100 || eventID % 100 == 0) {
    G4cout << ">>> Event: " << eventID  << G4endl;
    if ( trajectoryContainer ) {
      G4cout << "    " << n_trajectories
             << " trajectories stored in this event." << G4endl;
    }
     }

 G4VHitsCollection* hc = event->GetHCofThisEvent()->GetHC(0);

 G4int nhit=hc->GetSize();

//G4cout<<eventID<<"  "<<n_trajectories<<"  "<<nhit<<G4endl;
G4double Edep=0;
G4double LayerX[9]={-500,-500,-500,-500,-500,-500,-500,-500,-500};
G4double LayerY[9]={-500,-500,-500,-500,-500,-500,-500,-500,-500};
G4int nlayer=0;
for ( G4int i=0; i<nhit; i++ ) {
    PolarTrackerHit *hit = static_cast<PolarTrackerHit *>(hc->GetHit(i));
    Edep += hit->GetEdep();

if(hit->GetParentID()==0) {
    LayerX[hit->GetLayerNb()-1] = hit->GetPos().x()/10;
    LayerY[hit->GetLayerNb()-1] = hit->GetPos().y()/10;
   nlayer++;
}

}

if(nlayer==0) return;

auto analysisManager = G4AnalysisManager::Instance();
analysisManager->FillNtupleIColumn(0,0,eventID);
analysisManager->FillNtupleIColumn(0,1,n_trajectories);
analysisManager->FillNtupleIColumn(0,2,nhit);
analysisManager->FillNtupleDColumn(0,3,Edep);
    analysisManager->FillNtupleDColumn(0,4,vertexX);
    analysisManager->FillNtupleDColumn(0,5,vertexY);
    analysisManager->FillNtupleDColumn(0,6,vertexZ);
    for(G4int ibro=0;ibro<9;ibro++) {
        analysisManager->FillNtupleDColumn(0, 7+ibro*2, LayerX[ibro]);
        analysisManager->FillNtupleDColumn(0, 8+ibro*2, LayerY[ibro]);
    }
analysisManager->AddNtupleRow(0);
//    G4cout<<"endddd"<<G4endl;
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
