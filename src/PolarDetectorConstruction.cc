#include "PolarDetectorConstruction.hh"
#include "PolarTrackerSD.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4SDManager.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4AutoDelete.hh"
#include "G4GeometryTolerance.hh"
#include "G4FieldManager.hh"
#include "G4UserLimits.hh"
#include "G4UniformMagField.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4SystemOfUnits.hh"
#include "G4MagneticField.hh"

#include "G4TransportationManager.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
PolarDetectorConstruction::PolarDetectorConstruction()
:G4VUserDetectorConstruction(), 
 fNbOfLayers(0),
 fLogicTarget(NULL), fLogicLayer(NULL),
flogicInner(NULL),
 flogicMagnetic(NULL),
 fStepLimit(NULL)
// fCheckOverlaps(true)
{
  fNbOfLayers = 9;
  fLogicLayer = new G4LogicalVolume*[fNbOfLayers];
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
PolarDetectorConstruction::~PolarDetectorConstruction()
{
  delete [] fLogicLayer; 
  delete fStepLimit;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
 
G4VPhysicalVolume* PolarDetectorConstruction::Construct()
{
//    G4cout<<" PolarDetectorConstruction::Construct"<<G4endl;
// Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();
  
  G4bool checkOverlaps = true;

  // World
  //
  G4double world_sizeXY = 500*cm;
  G4double world_sizeZ  = 500*cm;
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_Galactic");
  
  G4Box* solidWorld =    
    new G4Box("World",                       //its name
       0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);     //its size
      
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        world_mat,           //its material
                        "World");            //its name
                                   
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(0,0,0),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "World",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      checkOverlaps);        //overlaps checking
                     

  // Magnetic
  //
 G4double Magnetic_R = 50*cm;
  G4double Magnetic_sizeZ  = 50*cm;
  G4Material* Inner_mat = nist->FindOrBuildMaterial("G4_Galactic");

G4Tubs* solidInner =
    new G4Tubs("Inner",                    //its name
        0,Magnetic_R,Magnetic_sizeZ,0.*deg, 360.*deg);//its size
      
 flogicInner =
    new G4LogicalVolume(solidInner,            //its solid
                        Inner_mat,             //its material
                        "Inner");         //its name
               
  new G4PVPlacement(0,                       //no rotation
                    G4ThreeVector(0,0,0*cm),         //at (0,0,0)
                    flogicInner,                //its logical volume
                    "Inner",              //its name
                    logicWorld,              //its mother  volume
                    false,                   //no boolean operation
                    0,                       //copy number
                    true);          //overlaps checkingi

 // G4Material* Magnetic_mat = nist->FindOrBuildMaterial("G4_Fe");
    G4Material* Magnetic_mat = nist->FindOrBuildMaterial("G4_Galactic");
 G4Tubs* solidMagnetic =    
    new G4Tubs("Magnetic",                    //its name
        (50-2.5)*cm,Magnetic_R,Magnetic_sizeZ,0.*deg, 360.*deg);//its size
      
     flogicMagnetic =
    new G4LogicalVolume(solidMagnetic,            //its solid
                        Magnetic_mat,             //its material
                        "Magnetic");         //its name
               
  new G4PVPlacement(0,                       //no rotation
                    G4ThreeVector(0,0,0*cm),         //at (0,0,0)
                   flogicMagnetic,
                    "Magnetic",              //its name
                    flogicInner,              //its mother  volume
                    false,                   //no boolean operation
                    0,                       //copy number
                    true);          //overlaps checkingi

//Layer1
 G4double L1R = 62*cm;
 G4double L1sizeZ  = 0.3*mm; 
BuildTrLayer(1,L1R,L1sizeZ,158.92*cm,logicWorld);

 G4double L2R = 62*cm;
 G4double L2sizeZ  = 0.3*mm; 
BuildTrLayer(2,L2R,L2sizeZ,53.06*cm,logicWorld);

 G4double L3R = 46*cm;
G4double L3sizeZ  = 0.3*mm; 
BuildTrLayer(3,L3R,L3sizeZ,29.228*cm,flogicInner);

 G4double L4R = 46*cm;
G4double L4sizeZ  = 0.3*mm; 
BuildTrLayer(4,L4R,L4sizeZ,25.212*cm,flogicInner);

 G4double L5R = 46*cm;
G4double L5sizeZ  = 0.3*mm; 
BuildTrLayer(5,L5R,L5sizeZ,1.698*cm,flogicInner);

 G4double L6R = 46*cm;
G4double L6sizeZ  = 0.3*mm; 
BuildTrLayer(6,L6R,L6sizeZ,-2.318*cm,flogicInner);

 G4double L7R = 46*cm;
G4double L7sizeZ  = 0.3*mm; 
BuildTrLayer(7,L7R,L7sizeZ,-25.212*cm,flogicInner);

 G4double L8R = 46*cm;
G4double L8sizeZ  = 0.3*mm; 
BuildTrLayer(8,L8R,L8sizeZ,-29.228*cm,flogicInner);

 G4double L9R = 61*cm;
G4double L9sizeZ  = 0.3*mm; 
BuildTrLayer(9,L9R,L9sizeZ,-135.882*cm,logicWorld);




return physWorld;

}



void  PolarDetectorConstruction::BuildTrLayer(G4int ilayer,G4double LayerR,G4double LayerZ,G4double Zposi,G4LogicalVolume* mothervolume){
  G4NistManager* nist = G4NistManager::Instance();
 // G4Material* Layer_mat = nist->FindOrBuildMaterial("G4_Si");

    G4Material* Layer_mat = nist->FindOrBuildMaterial("G4_Galactic");
  G4Tubs* solidLayer
    = new G4Tubs("Layer",0,LayerR,LayerZ, 0.*deg, 360.*deg);

  fLogicLayer[ilayer-1] =                         
    new G4LogicalVolume(solidLayer,            //its solid
                        Layer_mat,             //its material
                        "Layer");         //its name


 new G4PVPlacement(0,                       //no rotation
                    G4ThreeVector(0,0,Zposi),         //at (0,0,0)
                    fLogicLayer[ilayer-1],                //its logical volume
                    "Layer",              //its name
                    mothervolume,              //its mother  volume
                    false,                   //no boolean operation
                    ilayer,                       //copy number
                    true);          //overlaps checking

  G4VisAttributes* VisAtt = new G4VisAttributes(G4Colour(1.0,1.0,0.0));
  fLogicLayer[ilayer-1]->SetVisAttributes(VisAtt);

  G4double maxStep = 0.5*LayerZ;
  fStepLimit = new G4UserLimits(maxStep);
   fLogicLayer[ilayer-1]->SetUserLimits(fStepLimit);

}


void PolarDetectorConstruction::ConstructSDandField()
{

 G4String trackerLayerSDname = "Polarimeter/TrackerLayerSD";
  PolarTrackerSD* TrackerSD = new PolarTrackerSD(trackerLayerSDname,
                                            "TrackerHitsCollection");
  G4SDManager::GetSDMpointer()->AddNewDetector(TrackerSD);
    SetSensitiveDetector("Layer", TrackerSD, true);

/*G4MagneticField *magField;
magField = new G4UniformMagField(G4ThreeVector(0.14*tesla,0,0));

G4FieldManager* fieldMgr=new G4FieldManager(magField);
   
G4bool allLocal = true;
flogicInner->SetFieldManager(fieldMgr,allLocal);
*/


}

 

