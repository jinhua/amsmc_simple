#!/bin/bash
wd=/afs/cern.ch/work/j/jinhua/Polarimeter/build/
source /afs/cern.ch/work/j/jinhua/Geant4/install/bin/geant4.sh
let njob=$1+1
$wd/Polarimeter $wd/run1.mac $njob
cp *root /eos/ams/user/j/jinhua/GeomAcce
