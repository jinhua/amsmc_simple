#include "PolarDetectorConstruction.hh"
#include "PolarActionInitialization.hh"

#include "G4RunManagerFactory.hh"
#include "G4PhysListFactory.hh"

#include "G4UImanager.hh"
#include "FTFP_BERT.hh"
#include "G4StepLimiterPhysics.hh"

#include "Randomize.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main(int argc,char** argv)
{
  // Detect interactive mode (if no arguments) and define UI session
  //
  G4UIExecutive* ui = 0;
  if ( argc == 1 ) {
    ui = new G4UIExecutive(argc, argv);
  }

  // Optionally: choose a different Random engine...
 //  G4Random::setTheEngine(new CLHEP::MTwistEngine);
  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine());
      G4long seed = time(NULL);
          CLHEP::HepRandom::setTheSeed(seed);
  //
  auto* runManager =
    G4RunManagerFactory::CreateRunManager(G4RunManagerType::Default);

  // Set mandatory initialization classes
  //
  runManager->SetUserInitialization(new PolarDetectorConstruction());

 G4PhysListFactory factory;
    G4VModularPhysicsList* physlist = factory.GetReferencePhysList("FTFP_BERT__LE");
    physlist->RegisterPhysics(new G4StepLimiterPhysics());
    runManager->SetUserInitialization(physlist);
    
  // Set user action classes
G4String name;
    if ( argc == 3 ) {
        name=argv[2];
    }
    else  name="jinhua";

  runManager->SetUserInitialization(new PolarActionInitialization(name));
  
  // Initialize visualization
  //
  G4VisManager* visManager = new G4VisExecutive;
  // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
  // G4VisManager* visManager = new G4VisExecutive("Quiet");
  visManager->Initialize();

  // Get the pointer to the User Interface manager
  G4UImanager* UImanager = G4UImanager::GetUIpointer();

  // Process macro or start UI session
  //
  if ( ! ui ) { 
    // batch mode
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
    UImanager->ApplyCommand(command+fileName);
  }
  else { 
    // interactive mode
    UImanager->ApplyCommand("/control/execute init_vis.mac");
   if (ui->IsGUI()) {
      UImanager->ApplyCommand("/control/execute gui.mac");
    }
    ui->SessionStart();
    delete ui;
  }

  // Job termination
  // Free the store: user actions, physics_list and detector_description are
  // owned and deleted by the run manager, so they should not be deleted
  // in the main() program !
  //
  delete visManager;
  delete runManager;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
